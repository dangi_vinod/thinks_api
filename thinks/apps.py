from django.apps import AppConfig


class ThinksConfig(AppConfig):
    name = 'thinks'
