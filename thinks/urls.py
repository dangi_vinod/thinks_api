from django.urls import path
from thinks import views

urlpatterns = [
    path('', views.inventory),
    path('inventory_details/<int:pk>', views.inventory_detail)
]